package ithillel.com;

public class Cat extends Animals {
    private static int catCount = 0;

    public Cat(String name) {
        super(name);
        catCount++;
    }

    @Override
    public void run(int dist) {
        if (dist > 200) {
            System.out.println(getName() + " cant run " + dist);
        }
        super.run(dist);
    }

    @Override
    public void swim(int dist) {
        System.out.println(getName() + " cant swim " + dist);
    }

    public static int getCatCount() {
        return catCount;
    }
}
