package ithillel.com;

public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat("Bob");
        cat.run(22);
        cat.swim(1);
        Cat cat1 = new Cat("Bob");
        cat1.run(22);
        cat1.swim(1);
        System.out.println(Cat.getCatCount());
        System.out.println(Animals.getAnimalCount());
        Dog dog = new Dog("Jim");
        dog.run(2);
        dog.run(22);
        System.out.println(Dog.getDogCount());
        System.out.println(Animals.getAnimalCount());
    }
}
