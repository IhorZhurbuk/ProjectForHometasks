package ithillel.com;

public class Animals {
    private String name;
    private static int animalCount = 0;

    public Animals(String name) {
        this.name = name;
        animalCount++;
    }

    public String getName() {
        return name;
    }

    public static int getAnimalCount() {
        return animalCount;
    }

    public void run(int dist) {
        System.out.println(name + " run " + dist + " m.");
    }

    public void swim(int dist) {
        System.out.println(name + " swim ");
    }
}
