package ithillel.com;

public class Dog extends Animals {
    private static int dogCount = 0;

    public Dog(String name) {
        super(name);
        dogCount++;
    }

    @Override
    public void run(int dist) {
        if (dist > 500) {
            System.out.println(getName() + " cant run " + dist + " m.");
        }
        super.run(dist);
    }

    @Override
    public void swim(int dist) {
        if (dist > 10) {
            System.out.println(getName() + " cant swim " + dist + " m.");
        }
        super.swim(dist);
    }

    public static int getDogCount() {
        return dogCount;
    }
}
